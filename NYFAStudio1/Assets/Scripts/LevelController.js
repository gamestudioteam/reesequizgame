﻿#pragma strict
import UnityEngine.UI; // Needed for ui text to work

static var pickedUp = 0;
static var currentLevel = 1;
function Awake(){
    DontDestroyOnLoad(gameObject);
    
    }

var needed = 3; 
var scoreText : Text;


function Update () {

    var scoreObject = GameObject.FindWithTag("Scoreboard");
    
    if (scoreObject != null){
    
        scoreText = scoreObject.GetComponent(Text);
        
        if (pickedUp == needed){
        
        var scoreString = pickedUp.ToString() + " / " + needed.ToString() + " ...You answered everything correctly!";
        scoreText.text = scoreString;
        }
        
        else {
        
        scoreString = pickedUp.ToString() + " / " + needed.ToString() + " ...You missed something...";
        scoreText.text = scoreString;
        
        }
        
        if (pickedUp == needed) {
        
            var finalImage = GameObject.FindWithTag("good");
            finalImage.transform.position.y=5.5;
            }
            
        else {
        
            finalImage = GameObject.FindWithTag("bad");
            finalImage.transform.position.y=5.5;
            }
        
    }
}