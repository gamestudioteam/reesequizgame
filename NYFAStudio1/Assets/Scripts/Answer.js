﻿#pragma strict
public  var isCorrect:boolean;

function OnMouseEnter () {
        renderer.material.color = Color.yellow;
}

function OnMouseExit () {
        renderer.material.color = Color.white;
}

function OnMouseDown () {
        renderer.material.color = Color.red;        
}

function OnMouseUp () {
	if (isCorrect==true) {
        // increase the number of picked up items in the static class
        LevelController.pickedUp = LevelController.pickedUp + 1;
}
    
    if (LevelController.currentLevel == 1){
		Application.LoadLevel("Scene1");  
	} else if (LevelController.currentLevel == 2){
		Application.LoadLevel("Scene2");
	} else if (LevelController.currentLevel == 3){
		Application.LoadLevel("Scene3");
	} else if (LevelController.currentLevel == 4){
		Application.LoadLevel("End");
	} else if (LevelController.currentLevel == 5){
		LevelController.currentLevel = 0;
		Application.LoadLevel("Open");
	}
	LevelController.currentLevel = LevelController.currentLevel + 1;
}